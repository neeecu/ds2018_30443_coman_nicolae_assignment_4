package web;

import dao.PackageDao;
import dao.StatusDao;
import dao.UserDao;
import model.AppUser;
import model.Package;
import model.Status;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@WebService()
public class AdminServices {

    @WebMethod
    public Package makePackage(String sender, String receiver, String name, String description,
                               String from, String to) {
        AppUser sendingUser = UserDao.findByName(sender);
        AppUser receivingUser = UserDao.findByName(receiver);
        Package toInsert = new Package(sendingUser.getId(), receivingUser.getId(), name, description, from, to);
        PackageDao.insert(toInsert);
        return PackageDao.findByName(name);
    }

    @WebMethod
    public Status addStatusToPAck(String cityName, String time, String packageName) {
        Package pack = PackageDao.findByName(packageName);
        Timestamp timestamp = Timestamp.from(Instant.now());
        Status toAdd = new Status(cityName, timestamp);
        toAdd.setPackageId(pack.getId());
        return StatusDao.addStatusForPackage(toAdd);
    }

    @WebMethod
    public List<AppUser> getALlUsers (){
        return UserDao.getALl();
    }

    @WebMethod
    public Package makeTrackable(String name) {

        return PackageDao.makeTrackable(name);
    }

    @WebMethod
    public List<Package> getAllPackages(){
        return PackageDao.getAll();
    }


    public static void main(String[] argv) {
        Object implementor = new AdminServices();
        String address = "http://localhost:9001/AdminServices";
        Endpoint.publish(address, implementor);
    }
}
