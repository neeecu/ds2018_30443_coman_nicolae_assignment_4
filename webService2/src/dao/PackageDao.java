package dao;

import model.Package;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PackageDao {

    private final static String insertStatementString = "INSERT INTO package (sender, receiver, name, description, fromC, toC, tracking) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private final static String findByNameStatementString = "SELECT * FROM package where name = ?";
    private final static String updateStatementString = "UPDATE package SET tracking = ? WHERE name = ?";
    private final static String getAllStatementString = "SELECT * FROM package";

    public static Package insert(Package toInsert) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        ResultSet resultSet = null;
        int successful = 0;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setInt(1, toInsert.getSender());
            insertStatement.setInt(2, toInsert.getReceiver());
            insertStatement.setString(3, toInsert.getName());
            insertStatement.setString(4, toInsert.getDescription());
            insertStatement.setString(5, toInsert.getFromC());
            insertStatement.setString(6, toInsert.getToC());
            insertStatement.setBoolean(7, false);
            successful = insertStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return successful > 0 ? toInsert : null;
    }


    public static Package findByName(String name) {
        Package toReturn = null;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findByNameStatementString);
            findStatement.setString(1, name);
            rs = findStatement.executeQuery();
            rs.next();

            int id = rs.getInt("id");
            int sender = rs.getInt("sender");
            int receiver = rs.getInt("receiver");
            String description = rs.getString("description");
            String from = rs.getString("fromC");
            String to = rs.getString("toC");
            boolean tracking = rs.getBoolean("tracking");

            toReturn = new Package(sender, receiver, name, description, from, to);
            toReturn.setId(id);
            toReturn.setTracking(tracking);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static Package makeTrackable(String name) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;

        try {
            updateStatement = dbConnection.prepareStatement(updateStatementString);
            updateStatement.setBoolean(1, true);
            updateStatement.setString(2, name);
            updateStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
        return findByName(name);
    }

    public static List<Package> getAll(){
        List<Package> toReturn = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(getAllStatementString);
            rs = findStatement.executeQuery();
            while(rs.next()) {

                Package toAdd = new Package();
                int id = rs.getInt("id");
                int sender = rs.getInt("sender");
                int receiver = rs.getInt("receiver");
                String description = rs.getString("description");
                String name = rs.getString("name");
                String from = rs.getString("fromC");
                String to = rs.getString("toC");
                boolean tracking = rs.getBoolean("tracking");

                toAdd = new Package(sender, receiver, name, description, from, to);
                toAdd.setId(id);
                toAdd.setTracking(tracking);
                toReturn.add(toAdd);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }

        return toReturn;

    }

}
