package dao;

import model.AppUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {

    private final static String findByIdStatementString = "SELECT * FROM user where id = ?";
    private final static String findByNameStatementString = "SELECT * FROM user where username = ?";
    private final static String getALlUsersStatementString = "SELECT * FROM user";


    public static AppUser findById(int userId) {
        AppUser toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findByIdStatementString);
            findStatement.setLong(1, userId);
            rs = findStatement.executeQuery();
            rs.next();

            String username = rs.getString("username");
            String password = rs.getString("password");
            String role = rs.getString("role");
            String email = rs.getString("email");


            toReturn = new AppUser(userId, username, password, email, role);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static AppUser findByName(String username) {
        AppUser toReturn = null;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findByNameStatementString);
            findStatement.setString(1, username);
            rs = findStatement.executeQuery();
            rs.next();

            String password = rs.getString("password");
            String role = rs.getString("role");
            String email = rs.getString("email");
            int userId = rs.getInt("id");

            toReturn = new AppUser(userId, username, password, email, role);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }



    public static List<AppUser> getALl() {
        List<AppUser> toReturn = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement getStatement = null;
        ResultSet rs = null;
        try {
            getStatement = dbConnection.prepareStatement(getALlUsersStatementString);
            rs = getStatement.executeQuery();
            while (rs.next()) {
                AppUser user = new AppUser();
                user.setId(rs.getInt("id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setRole(rs.getString("role"));
                toReturn.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(getStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn.isEmpty() ? null : toReturn;
    }

}
