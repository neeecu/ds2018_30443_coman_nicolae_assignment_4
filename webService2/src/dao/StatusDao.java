package dao;

import model.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StatusDao {

    public final static String insertStatementString = "INSERT into status (city, time, package_id) VALUES (?, ?, ?)";


    public static Status addStatusForPackage(Status toInsert) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        ResultSet resultSet = null;
        int successful = 0;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setString(1, toInsert.getCity());
            insertStatement.setTimestamp(2, toInsert.getTime());
            insertStatement.setInt(3, (toInsert.getPackageId()));

            successful = insertStatement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return successful > 0 ? toInsert : null;

    }

}
