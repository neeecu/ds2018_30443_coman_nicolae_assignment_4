package model;

public class Package {
    private int id;
    private int sender;
    private int receiver;
    private String name;
    private String description;
    private String fromC;
    private String toC;
    private boolean tracking;

    public Package(int sender, int receiver, String name, String description, String from, String to) {
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
        this.description = description;
        this.fromC = from;
        this.toC = to;
    }

    public Package() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSender() {
        return sender;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }

    public int getReceiver() {
        return receiver;
    }

    public void setReceiver(int receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFromC() {
        return fromC;
    }

    public void setFromC(String fromC) {
        this.fromC = fromC;
    }

    public String getToC() {
        return toC;
    }

    public void setToC(String toC) {
        this.toC = toC;
    }

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }
}
