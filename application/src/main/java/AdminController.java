import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import webservice2.AppUser;
import webservice2.Package;
import webservice2.Status;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AdminController implements Initializable {

    @FXML
    private Pane thisPane;

    @FXML
    private Button addPackageButton;

    @FXML
    private Button addStatusButton;

    @FXML
    private TextField nameField;

    @FXML
    private TextField descriptionField;

    @FXML
    private TextField fromField;

    @FXML
    private TextField toField;

    @FXML
    private ComboBox<String> senderBox;

    @FXML
    private ComboBox<String> receiverBox;

    @FXML
    private ComboBox<String> trackableBox;

    @FXML
    private Button makeTrackableButton;

    @FXML
    private ComboBox<String> nonTrackableBox;

    @FXML
    private TextField statusCityField;

    private String selectedTrackable;
    private String selectedNonTrackable;
    private String selectedSender;
    private String selectedReceiver;

    List<Package> allPackages = new ArrayList<>();
    List<AppUser> appUsers = new ArrayList<>();

    @FXML
    void addPackage(ActionEvent event) {
        if (selectedSender.isEmpty() || selectedReceiver.isEmpty()
                || nameField.getText().isEmpty() || descriptionField.getText().isEmpty()
                || fromField.getText().isEmpty() || toField.getText().isEmpty()) {
            App.showAlert("Please fill in all the fields in order to add" +
                    "a package");
            return;
        }

        if (selectedSender.equals(selectedReceiver)) {
            App.showAlert("The sender cannot be the same as the receiver");
            return;
        }

        Package inserted = App.adminServices.makePackage(selectedSender, selectedReceiver, nameField.getText(), descriptionField.getText(),
                fromField.getText(), toField.getText());

        if (inserted != null) {
            App.showAlert("Insertion Successful");
            updateDate();
        } else {
            App.showAlert("Cannot insert package");

        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        updateDate();
    }

    @FXML
    void addStatus(ActionEvent event) {
        if (selectedTrackable.isEmpty()) {
            App.showAlert("Please pick a package that is trackable");
            return;
        }
        if (statusCityField.getText().isEmpty()) {
            App.showAlert("Please type in the city at which the package is right now");
        }
        Status status = App.adminServices
                .addStatusToPAck(statusCityField.getText(), "mocked", selectedTrackable);
        if (status != null) {
            App.showAlert("Status added successfully");
            updateDate();
        } else {
            App.showAlert("Cannot add status");
        }
    }

    @FXML
    void makeTrackable(ActionEvent event) {
        if (selectedNonTrackable.isEmpty()) {
            App.showAlert("Please pick a package that is not trackable");
            return;
        }
        Package pack = App.adminServices.makeTrackable(selectedNonTrackable);
        if (pack != null) {
            App.showAlert("Package made trackable");
            updateDate();
        } else {
            App.showAlert("Cannot make trackable");
        }
    }

    @FXML
    void nonTrackableClick(ActionEvent event) {
        selectedNonTrackable = nonTrackableBox.getSelectionModel().getSelectedItem();
    }

    @FXML
    void receiverBoxClick(ActionEvent event) {
        selectedReceiver = receiverBox.getSelectionModel().getSelectedItem();
    }

    @FXML
    void senderBoxClick(ActionEvent event) {
        selectedSender = senderBox.getSelectionModel().getSelectedItem();
    }

    @FXML
    void trackableBoxClick(ActionEvent event) {
        selectedTrackable = trackableBox.getSelectionModel().getSelectedItem();
    }

    /*java streams not working wtf ?*/
    public void updateDate() {
        clearBoxes();
        appUsers = App.adminServices.getALlUsers();
        allPackages = App.adminServices.getAllPackages();

        for (AppUser user : appUsers) {
            receiverBox.getItems().add(user.getUsername());
            senderBox.getItems().add(user.getUsername());
        }

        for (Package pack : allPackages) {
            if (pack.isTracking()) {
                trackableBox.getItems().add(pack.getName());
            } else {
                nonTrackableBox.getItems().add(pack.getName());
            }
        }
    }

    private void clearBoxes() {
        senderBox.getItems().clear();
        receiverBox.getItems().clear();
        trackableBox.getItems().clear();
        nonTrackableBox.getItems().clear();
    }
}
