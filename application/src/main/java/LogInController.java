import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class LogInController {

    @FXML
    private Pane thisPane;

    @FXML
    private Button logInButton;

    @FXML
    private TextField usernameTextField;

    @FXML
    private PasswordField passwordTextField;

    @FXML
    private Button signUpButton;


    //x and y offsets used to implement handlers for mouse draggable frames
    private double xOffset = 0.0;
    private double yOffset = 0.0;


    //close the window and app if right mouse click is pressed in this frame
    @FXML
    void logInClick(ActionEvent event) throws IOException {

        String username = usernameTextField.getText();
        String password = passwordTextField.getText();
        String response = App.userServices.login(username, password);

        if (response.equals("USER")) {
            startUserFrame();
        } else if (response.equals("ADMIN")) {
            startAdminFrame();
        } else {
            App.showAlert(response);
        }

    }

    @FXML
    void registerButtonClick(ActionEvent event) throws IOException {
        changeView("Register.fxml", "Register");
    }

    private void startUserFrame() throws IOException {
        changeView("ClientView.fxml", "User");
    }

    private void startAdminFrame() throws IOException {
        changeView("AdminView.fxml", "Admin");
    }


    private void changeView(String fxmlPath, String windowName) throws IOException {
        //load the sign up frame
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlPath));
        Pane nextPane = loader.load();

        //hide the log in frame
        final Stage currentStage = (Stage) thisPane.getScene().getWindow();
        currentStage.hide();

        //set up the stage //need final modifier for the handler methods
        final Stage nextStage = new Stage();


        //remove registerStage Borders
        nextStage.initStyle(StageStyle.UNDECORATED);
        nextStage.setTitle(windowName);

        //make the frame draggable


        nextPane.setOnMousePressed(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        nextPane.setOnMouseDragged(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                nextStage.setX(event.getScreenX() - xOffset);
                nextStage.setY(event.getScreenY() - yOffset);
            }
        });


        nextPane.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                MouseButton pressedButton = event.getButton();
                if (pressedButton == MouseButton.SECONDARY) {
                    //close the registration frame and show the log in one
                    nextStage.close();
                    currentStage.show();
                }
            }
        });

        nextStage.setScene(new Scene(nextPane));

        if (windowName.equals("User")) {
            UserController controller = loader.<UserController>
                    getController();
            controller.setCurrentUser(usernameTextField.getText());
        }

        //show the new frame
        nextStage.show();
    }

}