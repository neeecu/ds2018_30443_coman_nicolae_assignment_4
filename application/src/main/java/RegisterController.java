import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class RegisterController {

    @FXML
    private TextField nameField;

    @FXML
    private PasswordField passField;

    @FXML
    private TextField emailField;

    @FXML
    private Button registerButton;

    @FXML
    public void registerButtonClick(ActionEvent event) {
        String username = nameField.getText();
        String password = passField.getText();
        String email = emailField.getText();
        if (!username.isEmpty() && !password.isEmpty() && !email.isEmpty()) {
            App.userServices.register(username, password, email, "USER");
        } else {
            App.showAlert("Please fill in all the fields");
        }
    }
}
