import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import webservice1.Package;
import webservice1.Status;

import java.util.List;

public class PackageTableController {

    @FXML
    private TableView<Package> packageTable;

    @FXML
    private Button showButton;

    private String currentUser;

    @FXML
    void showPacks(ActionEvent event) {

        packageTable.getColumns().clear();
        packageTable.getItems().clear();

        ObservableList<Package> data = FXCollections.
                observableArrayList( App.userServices.getAllPackages(currentUser).getPackage());

        packageTable.getItems().setAll(data);
        TableColumn sender = new TableColumn("sender");
        sender.setCellValueFactory(new PropertyValueFactory<Package, String>("Sender"));
        TableColumn receiver = new TableColumn("receiver");
        receiver.setCellValueFactory(new PropertyValueFactory<Package, String>("Receiver"));
        TableColumn packageName = new TableColumn("P.Name");
        packageName.setCellValueFactory(new PropertyValueFactory<Package, String>("Name"));

        TableColumn description = new TableColumn("description");
        description.setCellValueFactory(new PropertyValueFactory<Package, String>("Description"));
        TableColumn from = new TableColumn("from");
        from.setCellValueFactory(new PropertyValueFactory<Package, String>("From"));
        TableColumn to = new TableColumn("to");
        to.setCellValueFactory(new PropertyValueFactory<Package, String>("To"));
        packageTable.getColumns().addAll(sender, receiver, packageName, description, from, to);

    }

    @FXML
    void tableClick(MouseEvent event) {
        Package pack =  packageTable.getSelectionModel().selectedItemProperty().get();
        List<Status> path = App.userServices.getPathForPackage(pack.getId()).getStatus();
        String pathString = buildPathString(path);
        App.showAlert("Path:\n"+pathString);
    }

    private String buildPathString(List<Status> path) {
        String pathString = "";
        for(Status s: path){
            pathString+="("+s.getCity()+", "+s.getDateTime()+")\n";
        }
        return pathString;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }
}
