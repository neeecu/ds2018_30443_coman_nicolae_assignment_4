
package webservice2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservice2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAllPackages_QNAME = new QName("http://web/", "getAllPackages");
    private final static QName _GetALlUsers_QNAME = new QName("http://web/", "getALlUsers");
    private final static QName _GetAllPackagesResponse_QNAME = new QName("http://web/", "getAllPackagesResponse");
    private final static QName _MakePackage_QNAME = new QName("http://web/", "makePackage");
    private final static QName _AddStatusToPAck_QNAME = new QName("http://web/", "addStatusToPAck");
    private final static QName _AddStatusToPAckResponse_QNAME = new QName("http://web/", "addStatusToPAckResponse");
    private final static QName _MakeTrackable_QNAME = new QName("http://web/", "makeTrackable");
    private final static QName _MakeTrackableResponse_QNAME = new QName("http://web/", "makeTrackableResponse");
    private final static QName _GetALlUsersResponse_QNAME = new QName("http://web/", "getALlUsersResponse");
    private final static QName _MakePackageResponse_QNAME = new QName("http://web/", "makePackageResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservice2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetALlUsersResponse }
     * 
     */
    public GetALlUsersResponse createGetALlUsersResponse() {
        return new GetALlUsersResponse();
    }

    /**
     * Create an instance of {@link MakePackageResponse }
     * 
     */
    public MakePackageResponse createMakePackageResponse() {
        return new MakePackageResponse();
    }

    /**
     * Create an instance of {@link AddStatusToPAck }
     * 
     */
    public AddStatusToPAck createAddStatusToPAck() {
        return new AddStatusToPAck();
    }

    /**
     * Create an instance of {@link AddStatusToPAckResponse }
     * 
     */
    public AddStatusToPAckResponse createAddStatusToPAckResponse() {
        return new AddStatusToPAckResponse();
    }

    /**
     * Create an instance of {@link MakeTrackable }
     * 
     */
    public MakeTrackable createMakeTrackable() {
        return new MakeTrackable();
    }

    /**
     * Create an instance of {@link MakeTrackableResponse }
     * 
     */
    public MakeTrackableResponse createMakeTrackableResponse() {
        return new MakeTrackableResponse();
    }

    /**
     * Create an instance of {@link GetAllPackagesResponse }
     * 
     */
    public GetAllPackagesResponse createGetAllPackagesResponse() {
        return new GetAllPackagesResponse();
    }

    /**
     * Create an instance of {@link GetALlUsers }
     * 
     */
    public GetALlUsers createGetALlUsers() {
        return new GetALlUsers();
    }

    /**
     * Create an instance of {@link MakePackage }
     * 
     */
    public MakePackage createMakePackage() {
        return new MakePackage();
    }

    /**
     * Create an instance of {@link GetAllPackages }
     * 
     */
    public GetAllPackages createGetAllPackages() {
        return new GetAllPackages();
    }

    /**
     * Create an instance of {@link AppUser }
     * 
     */
    public AppUser createAppUser() {
        return new AppUser();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link Timestamp }
     * 
     */
    public Timestamp createTimestamp() {
        return new Timestamp();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPackages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "getAllPackages")
    public JAXBElement<GetAllPackages> createGetAllPackages(GetAllPackages value) {
        return new JAXBElement<GetAllPackages>(_GetAllPackages_QNAME, GetAllPackages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetALlUsers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "getALlUsers")
    public JAXBElement<GetALlUsers> createGetALlUsers(GetALlUsers value) {
        return new JAXBElement<GetALlUsers>(_GetALlUsers_QNAME, GetALlUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPackagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "getAllPackagesResponse")
    public JAXBElement<GetAllPackagesResponse> createGetAllPackagesResponse(GetAllPackagesResponse value) {
        return new JAXBElement<GetAllPackagesResponse>(_GetAllPackagesResponse_QNAME, GetAllPackagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakePackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "makePackage")
    public JAXBElement<MakePackage> createMakePackage(MakePackage value) {
        return new JAXBElement<MakePackage>(_MakePackage_QNAME, MakePackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddStatusToPAck }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "addStatusToPAck")
    public JAXBElement<AddStatusToPAck> createAddStatusToPAck(AddStatusToPAck value) {
        return new JAXBElement<AddStatusToPAck>(_AddStatusToPAck_QNAME, AddStatusToPAck.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddStatusToPAckResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "addStatusToPAckResponse")
    public JAXBElement<AddStatusToPAckResponse> createAddStatusToPAckResponse(AddStatusToPAckResponse value) {
        return new JAXBElement<AddStatusToPAckResponse>(_AddStatusToPAckResponse_QNAME, AddStatusToPAckResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakeTrackable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "makeTrackable")
    public JAXBElement<MakeTrackable> createMakeTrackable(MakeTrackable value) {
        return new JAXBElement<MakeTrackable>(_MakeTrackable_QNAME, MakeTrackable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakeTrackableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "makeTrackableResponse")
    public JAXBElement<MakeTrackableResponse> createMakeTrackableResponse(MakeTrackableResponse value) {
        return new JAXBElement<MakeTrackableResponse>(_MakeTrackableResponse_QNAME, MakeTrackableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetALlUsersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "getALlUsersResponse")
    public JAXBElement<GetALlUsersResponse> createGetALlUsersResponse(GetALlUsersResponse value) {
        return new JAXBElement<GetALlUsersResponse>(_GetALlUsersResponse_QNAME, GetALlUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MakePackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web/", name = "makePackageResponse")
    public JAXBElement<MakePackageResponse> createMakePackageResponse(MakePackageResponse value) {
        return new JAXBElement<MakePackageResponse>(_MakePackageResponse_QNAME, MakePackageResponse.class, null, value);
    }

}
