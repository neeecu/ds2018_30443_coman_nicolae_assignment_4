
package webservice1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getUserByIdResult" type="{http://tempuri.org/}AppUser" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserByIdResult"
})
@XmlRootElement(name = "getUserByIdResponse")
public class GetUserByIdResponse {

    protected AppUser getUserByIdResult;

    /**
     * Gets the value of the getUserByIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link AppUser }
     *     
     */
    public AppUser getGetUserByIdResult() {
        return getUserByIdResult;
    }

    /**
     * Sets the value of the getUserByIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AppUser }
     *     
     */
    public void setGetUserByIdResult(AppUser value) {
        this.getUserByIdResult = value;
    }

}
