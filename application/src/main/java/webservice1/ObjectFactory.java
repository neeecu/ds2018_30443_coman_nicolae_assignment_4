
package webservice1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservice1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservice1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link GetUserById }
     * 
     */
    public GetUserById createGetUserById() {
        return new GetUserById();
    }

    /**
     * Create an instance of {@link GetUserByIdResponse }
     * 
     */
    public GetUserByIdResponse createGetUserByIdResponse() {
        return new GetUserByIdResponse();
    }

    /**
     * Create an instance of {@link AppUser }
     * 
     */
    public AppUser createAppUser() {
        return new AppUser();
    }

    /**
     * Create an instance of {@link GetPathForPackageResponse }
     * 
     */
    public GetPathForPackageResponse createGetPathForPackageResponse() {
        return new GetPathForPackageResponse();
    }

    /**
     * Create an instance of {@link ArrayOfStatus }
     * 
     */
    public ArrayOfStatus createArrayOfStatus() {
        return new ArrayOfStatus();
    }

    /**
     * Create an instance of {@link GetAllPackagesResponse }
     * 
     */
    public GetAllPackagesResponse createGetAllPackagesResponse() {
        return new GetAllPackagesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPackage }
     * 
     */
    public ArrayOfPackage createArrayOfPackage() {
        return new ArrayOfPackage();
    }

    /**
     * Create an instance of {@link GetPathForPackage }
     * 
     */
    public GetPathForPackage createGetPathForPackage() {
        return new GetPathForPackage();
    }

    /**
     * Create an instance of {@link CheckPackageStatus }
     * 
     */
    public CheckPackageStatus createCheckPackageStatus() {
        return new CheckPackageStatus();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link GetAllPackages }
     * 
     */
    public GetAllPackages createGetAllPackages() {
        return new GetAllPackages();
    }

    /**
     * Create an instance of {@link FindPackageResponse }
     * 
     */
    public FindPackageResponse createFindPackageResponse() {
        return new FindPackageResponse();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link FindPackage }
     * 
     */
    public FindPackage createFindPackage() {
        return new FindPackage();
    }

    /**
     * Create an instance of {@link RegisterResponse }
     * 
     */
    public RegisterResponse createRegisterResponse() {
        return new RegisterResponse();
    }

    /**
     * Create an instance of {@link CheckPackageStatusResponse }
     * 
     */
    public CheckPackageStatusResponse createCheckPackageStatusResponse() {
        return new CheckPackageStatusResponse();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link Register }
     * 
     */
    public Register createRegister() {
        return new Register();
    }

}
