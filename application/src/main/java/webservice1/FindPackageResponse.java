
package webservice1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="findPackageResult" type="{http://tempuri.org/}Package" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "findPackageResult"
})
@XmlRootElement(name = "findPackageResponse")
public class FindPackageResponse {

    protected Package findPackageResult;

    /**
     * Gets the value of the findPackageResult property.
     * 
     * @return
     *     possible object is
     *     {@link Package }
     *     
     */
    public Package getFindPackageResult() {
        return findPackageResult;
    }

    /**
     * Sets the value of the findPackageResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Package }
     *     
     */
    public void setFindPackageResult(Package value) {
        this.findPackageResult = value;
    }

}
