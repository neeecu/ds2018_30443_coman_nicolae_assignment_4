
package webservice1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPathForPackageResult" type="{http://tempuri.org/}ArrayOfStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPathForPackageResult"
})
@XmlRootElement(name = "getPathForPackageResponse")
public class GetPathForPackageResponse {

    protected ArrayOfStatus getPathForPackageResult;

    /**
     * Gets the value of the getPathForPackageResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfStatus }
     *     
     */
    public ArrayOfStatus getGetPathForPackageResult() {
        return getPathForPackageResult;
    }

    /**
     * Sets the value of the getPathForPackageResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfStatus }
     *     
     */
    public void setGetPathForPackageResult(ArrayOfStatus value) {
        this.getPathForPackageResult = value;
    }

}
