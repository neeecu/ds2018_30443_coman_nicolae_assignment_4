<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://tempuri.org/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" targetNamespace="http://tempuri.org/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://tempuri.org/">
      <s:element name="register">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="username" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="password" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="email" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="role" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="registerResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="registerResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="login">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="username" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="password" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="loginResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="loginResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getAllPackages">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="username" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getAllPackagesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getAllPackagesResult" type="tns:ArrayOfPackage" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfPackage">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="Package" nillable="true" type="tns:Package" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="Package">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Id" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="Sender" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="Receiver" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Description" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="From" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="To" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Tracking" type="s:boolean" />
        </s:sequence>
      </s:complexType>
      <s:element name="findPackage">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="packageName" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="findPackageResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="findPackageResult" type="tns:Package" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="checkPackageStatus">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="packageName" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="checkPackageStatusResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="checkPackageStatusResult" type="tns:Status" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="Status">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Id" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="City" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="DateTime" type="s:dateTime" />
          <s:element minOccurs="1" maxOccurs="1" name="Package" type="s:int" />
        </s:sequence>
      </s:complexType>
      <s:element name="getUserById">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="id" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getUserByIdResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getUserByIdResult" type="tns:AppUser" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="AppUser">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Id" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Username" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Password" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Email" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Role" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="getPathForPackage">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="packageId" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getPathForPackageResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getPathForPackageResult" type="tns:ArrayOfStatus" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfStatus">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="Status" nillable="true" type="tns:Status" />
        </s:sequence>
      </s:complexType>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="registerSoapIn">
    <wsdl:part name="parameters" element="tns:register" />
  </wsdl:message>
  <wsdl:message name="registerSoapOut">
    <wsdl:part name="parameters" element="tns:registerResponse" />
  </wsdl:message>
  <wsdl:message name="loginSoapIn">
    <wsdl:part name="parameters" element="tns:login" />
  </wsdl:message>
  <wsdl:message name="loginSoapOut">
    <wsdl:part name="parameters" element="tns:loginResponse" />
  </wsdl:message>
  <wsdl:message name="getAllPackagesSoapIn">
    <wsdl:part name="parameters" element="tns:getAllPackages" />
  </wsdl:message>
  <wsdl:message name="getAllPackagesSoapOut">
    <wsdl:part name="parameters" element="tns:getAllPackagesResponse" />
  </wsdl:message>
  <wsdl:message name="findPackageSoapIn">
    <wsdl:part name="parameters" element="tns:findPackage" />
  </wsdl:message>
  <wsdl:message name="findPackageSoapOut">
    <wsdl:part name="parameters" element="tns:findPackageResponse" />
  </wsdl:message>
  <wsdl:message name="checkPackageStatusSoapIn">
    <wsdl:part name="parameters" element="tns:checkPackageStatus" />
  </wsdl:message>
  <wsdl:message name="checkPackageStatusSoapOut">
    <wsdl:part name="parameters" element="tns:checkPackageStatusResponse" />
  </wsdl:message>
  <wsdl:message name="getUserByIdSoapIn">
    <wsdl:part name="parameters" element="tns:getUserById" />
  </wsdl:message>
  <wsdl:message name="getUserByIdSoapOut">
    <wsdl:part name="parameters" element="tns:getUserByIdResponse" />
  </wsdl:message>
  <wsdl:message name="getPathForPackageSoapIn">
    <wsdl:part name="parameters" element="tns:getPathForPackage" />
  </wsdl:message>
  <wsdl:message name="getPathForPackageSoapOut">
    <wsdl:part name="parameters" element="tns:getPathForPackageResponse" />
  </wsdl:message>
  <wsdl:portType name="WebService1Soap">
    <wsdl:operation name="register">
      <wsdl:input message="tns:registerSoapIn" />
      <wsdl:output message="tns:registerSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="login">
      <wsdl:input message="tns:loginSoapIn" />
      <wsdl:output message="tns:loginSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getAllPackages">
      <wsdl:input message="tns:getAllPackagesSoapIn" />
      <wsdl:output message="tns:getAllPackagesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="findPackage">
      <wsdl:input message="tns:findPackageSoapIn" />
      <wsdl:output message="tns:findPackageSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="checkPackageStatus">
      <wsdl:input message="tns:checkPackageStatusSoapIn" />
      <wsdl:output message="tns:checkPackageStatusSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getUserById">
      <wsdl:input message="tns:getUserByIdSoapIn" />
      <wsdl:output message="tns:getUserByIdSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getPathForPackage">
      <wsdl:input message="tns:getPathForPackageSoapIn" />
      <wsdl:output message="tns:getPathForPackageSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="WebService1Soap" type="tns:WebService1Soap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="register">
      <soap:operation soapAction="http://tempuri.org/register" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="login">
      <soap:operation soapAction="http://tempuri.org/login" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAllPackages">
      <soap:operation soapAction="http://tempuri.org/getAllPackages" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="findPackage">
      <soap:operation soapAction="http://tempuri.org/findPackage" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="checkPackageStatus">
      <soap:operation soapAction="http://tempuri.org/checkPackageStatus" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUserById">
      <soap:operation soapAction="http://tempuri.org/getUserById" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getPathForPackage">
      <soap:operation soapAction="http://tempuri.org/getPathForPackage" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="WebService1Soap12" type="tns:WebService1Soap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="register">
      <soap12:operation soapAction="http://tempuri.org/register" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="login">
      <soap12:operation soapAction="http://tempuri.org/login" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAllPackages">
      <soap12:operation soapAction="http://tempuri.org/getAllPackages" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="findPackage">
      <soap12:operation soapAction="http://tempuri.org/findPackage" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="checkPackageStatus">
      <soap12:operation soapAction="http://tempuri.org/checkPackageStatus" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUserById">
      <soap12:operation soapAction="http://tempuri.org/getUserById" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getPathForPackage">
      <soap12:operation soapAction="http://tempuri.org/getPathForPackage" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="WebService1">
    <wsdl:port name="WebService1Soap" binding="tns:WebService1Soap">
      <soap:address location="http://localhost:57581/WebService1.asmx" />
    </wsdl:port>
    <wsdl:port name="WebService1Soap12" binding="tns:WebService1Soap12">
      <soap12:address location="http://localhost:57581/WebService1.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>