import com.google.gson.Gson;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import webservice1.Package;
import webservice1.Status;

import java.io.IOException;

public class UserController {
    @FXML
    private Pane thisPane;

    @FXML
    private Button getPackagesButton;

    @FXML
    private Button searchPackageButton;

    @FXML
    private TextField packageNameField;

    @FXML
    private Button checkStatusButton;

    //x and y offsets used to implement handlers for mouse draggable frames
    private double xOffset = 0.0;
    private double yOffset = 0.0;
    private String currentUser;


    @FXML
    void checkStatus(ActionEvent event) {
        Status currentStatus = App.userServices.checkPackageStatus(packageNameField.getText());
        if(currentStatus == null){
            App.showAlert("No package found for the given name, or the package is not trackable");
        } else {
            App.showAlert("Last City: "+currentStatus.getCity() +", at date: "+currentStatus.getDateTime() );
        }
    }

    @FXML
    void getAllPackages(ActionEvent event) throws IOException {
        changeView("PackageTable.fxml", "Packages");
    }

    @FXML
    void searchPackage(ActionEvent event) {
        Gson gson = new Gson();
        Package packFound = App.userServices.findPackage(packageNameField.getText());
        if(packFound == null){
            App.showAlert("No package found for the given name");
        } else {
            App.showAlert(gson.toJson(packFound));
        }

    }

    private void changeView(String fxmlPath, String windowName) throws IOException {
        //load the sign up frame
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlPath));
        Pane nextPane = loader.load();

        //hide the log in frame
        final Stage currentStage = (Stage) thisPane.getScene().getWindow();
        currentStage.hide();

        //set up the stage //need final modifier for the handler methods
        final Stage nextStage = new Stage();


        //remove registerStage Borders
        nextStage.initStyle(StageStyle.UNDECORATED);
        nextStage.setTitle(windowName);

        //make the frame draggable


        nextPane.setOnMousePressed(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        nextPane.setOnMouseDragged(new EventHandler<MouseEvent>() {

            public void handle(MouseEvent event) {
                nextStage.setX(event.getScreenX() - xOffset);
                nextStage.setY(event.getScreenY() - yOffset);
            }
        });


        nextPane.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                MouseButton pressedButton = event.getButton();
                if (pressedButton == MouseButton.SECONDARY) {
                    //close the registration frame and show the log in one
                    nextStage.close();
                    currentStage.show();
                }
            }
        });


        nextStage.setScene(new Scene(nextPane));

        PackageTableController controller = loader.<PackageTableController>
                getController();
        controller.setCurrentUser(currentUser);

        //show the new frame
        nextStage.show();
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }
}
