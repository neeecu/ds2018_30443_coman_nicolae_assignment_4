﻿using System;

namespace WebService1.App_Code.api
{
    public class Status{

        private int id;
        public int Id { get; set; }

        private string city;
        public string City { get; set; }

        private DateTime dateTime;
        public DateTime DateTime { get; set; }

        private int package;
        public int Package { get; set;}
    }
}