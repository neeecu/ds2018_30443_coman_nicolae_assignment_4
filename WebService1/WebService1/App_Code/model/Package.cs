﻿namespace WebService1.App_Code.api
{
    public class Package {

        private int id;
        public int  Id {get; set; }

        private int sender;
        public int Sender { get; set; }

        private int receiver;
        public int Receiver { get; set; }

        private string name;
        public string Name { get; set; }

        private string description;
        public string Description { get; set; }

        private string from;
        public string From { get; set; }

        private string to;
        public string To { get; set; }

        private bool tracking;
        public bool Tracking { get; set; }

        public Package() { }

        public Package(int sender, int receiver, string name, string description, string from, string to, bool tracking)        {
            this.sender = sender;
            this.receiver = receiver;
            this.name = name;
            this.description = description;
            this.from = from;
            this.to = to;
            this.tracking = tracking;
        }

    }
}