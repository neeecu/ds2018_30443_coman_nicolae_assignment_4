﻿
namespace WebService1.App_Code.api
{
    public class AppUser
    {
        private int id;
        public int Id { get { return id; } set { this.id = value; } }

        private string username;
        public string Username { get { return username; } set { this.username = value; }  }

        private string password;
        public string Password { get { return password; }  set{ this.password = value; } } 

        private string email;
        public string Email { get { return email; } set { this.email = value; } }

        private string role;
        public string Role { get { return role; } set { this.role = value; } }

        public AppUser(string username, string password, string email, string role){
         
            this.username = username;
            this.password = password;
            this.email = email;
            this.role = role;
        }

        public AppUser() { }

    }
}