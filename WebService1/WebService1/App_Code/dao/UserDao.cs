﻿using System;
using MySql.Data.MySqlClient;

namespace WebService1.App_Code.api
{
    public class UserDao {

        private MySqlConnection conn;

        private const string CONNECTION_STRING = "server=localhost;user=root;database=assig4;port=3306;password=root";
        private const string insertStatementString = "INSERT INTO user (username,password,email,role)"
            + "VALUES (@username,@password,@email,@role)";
        private const string findByNameStatementString = "SELECT id, password, email, role FROM user WHERE username=@username";
        private const string findByIdStatementString = "SELECT *  FROM user WHERE id=@id";

        public UserDao() { }

        public string addUser(AppUser user) {
            this.conn = new MySqlConnection(CONNECTION_STRING);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(insertStatementString, conn);

            cmd.Parameters.AddWithValue("@username", user.Username);
            cmd.Parameters.AddWithValue("@password", user.Password);
            cmd.Parameters.AddWithValue("@email", user.Email);
            cmd.Parameters.AddWithValue("@role", user.Role);

            int rowsAffected = cmd.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected == 0) {
                return "Not Inserted, Error occured";
            }
            return "Inserted";
        }

        internal AppUser findUserById(int id)
        {
            this.conn = new MySqlConnection(CONNECTION_STRING);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(findByIdStatementString, conn);
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader result = cmd.ExecuteReader();


            if (result.Read())
            {
                AppUser toReturn = new AppUser();
                toReturn.Email = (string)result["email"];
                toReturn.Password = (string)result["password"];
                toReturn.Role = (string)result["role"];
                toReturn.Id = id;
                toReturn.Username = (string)result["username"];
                result.Close();
                return toReturn;
            }
            conn.Close();
            result.Close();

            return null;
        }
    

        public AppUser findUserByUsername(string username) {
            this.conn = new MySqlConnection(CONNECTION_STRING);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(findByNameStatementString, conn);
            cmd.Parameters.AddWithValue("@username", username);
            MySqlDataReader result = cmd.ExecuteReader();
            

            if (result.Read()) {
                AppUser toReturn = new AppUser();
                toReturn.Email = (string) result["email"];
                toReturn.Password = (string)result["password"];
                toReturn.Role = (string)result["role"];
                toReturn.Id = (int)result["id"];
                toReturn.Username = username;
                result.Close();
                return toReturn;
            }
            conn.Close();
            result.Close();
           
            return null;
        }

    }
}