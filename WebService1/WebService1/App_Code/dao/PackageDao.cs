﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace WebService1.App_Code.api
{
    public class PackageDao{

        private MySqlConnection conn;
        private const string getAllPackagesForUserStatementString = "SELECT * from package WHERE sender = @id1 OR receiver = @id2";
        private const string findByNameStatementString = "SELECT * FROM package WHERE name=@name";
        private const string findByIdStatementString = "SELECT * FROM package WHERE id=@id";

        private const string CONNECTION_STRING = "server=localhost;user=root;database=assig4;port=3306;password=root";

        public PackageDao() {}

        public List<Package> getAllForUser(string userName, int id) {
            conn = new MySqlConnection(CONNECTION_STRING);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(getAllPackagesForUserStatementString, conn);
            cmd.Parameters.AddWithValue("@id1",id);
            cmd.Parameters.AddWithValue("@id2", id);
            MySqlDataReader result = cmd.ExecuteReader();
            
            List<Package> packages = new List<Package>(100);
            while (result.Read()) {
                Package toAdd = new Package();
                toAdd.Sender = (int) result["sender"];
                toAdd.Receiver = (int)result["receiver"];
                toAdd.Name = (string)result["name"];
                toAdd.Id = (int)result["id"];
                toAdd.Description = (string)result["description"];
                toAdd.From = (string)result["fromC"];
                toAdd.To = (string)result["toC"];
                toAdd.Tracking = Convert.ToInt32(result["tracking"]) != 0;
                packages.Add(toAdd);
            }
            conn.Close();
            result.Close();

            return packages; 
        }

        public Package findByName(string packageName) {
            this.conn = new MySqlConnection(CONNECTION_STRING);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(findByNameStatementString, conn);
            cmd.Parameters.AddWithValue("@name", packageName);
            MySqlDataReader result = cmd.ExecuteReader();

            if (result.Read()){
                Package toReturn = new Package();
                toReturn.Id = (int)result["id"];
                toReturn.Sender = (int)result["sender"];
                toReturn.Receiver = (int)result["receiver"];
                toReturn.Name = packageName;
                toReturn.Description = (string)result["description"];
                toReturn.From = (string)result["fromC"];
                toReturn.To = (string)result["toC"];
                toReturn.Tracking = Convert.ToInt32(result["tracking"]) != 0;
                result.Close();
                return toReturn;
            }
            conn.Close();
            result.Close();

            return null;
        }

        public Package findById(int id)
        {
            this.conn = new MySqlConnection(CONNECTION_STRING);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(findByIdStatementString, conn);
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader result = cmd.ExecuteReader();

            if (result.Read())
            {
                Package toReturn = new Package();
                toReturn.Id = id;
                toReturn.Sender = (int)result["sender"];
                toReturn.Receiver = (int)result["receiver"];
                toReturn.Name = (string)result["name"];
                toReturn.Description = (string)result["description"];
                toReturn.From = (string)result["fromC"];
                toReturn.To = (string)result["toC"];
                toReturn.Tracking = Convert.ToInt32(result["tracking"]) != 0;
                result.Close();
                return toReturn;
            }
            conn.Close();
            result.Close();

            return null;
        }
    }
}