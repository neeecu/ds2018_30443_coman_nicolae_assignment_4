﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace WebService1.App_Code.api
{
    public class StatusDao {

        private MySqlConnection conn;
        private const string getPathForPackageStatementString = "SELECT * FROM status WHERE package_id=@id";

        private const string CONNECTION_STRING = "server=localhost;user=root;database=assig4;port=3306;password=root";

        public StatusDao () {}
        public List<Status> getPathForPackage(int packageId){
            this.conn = new MySqlConnection(CONNECTION_STRING);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(getPathForPackageStatementString, conn);
            cmd.Parameters.AddWithValue("@id", packageId);
            MySqlDataReader result = cmd.ExecuteReader();
            List<Status> path = new List<Status>(100);
            while (result.Read()) {
                Status toAdd = new Status();
                toAdd.Id = (int)result["id"];
                toAdd.City = (string)result["city"];
                toAdd.DateTime = (DateTime)result["time"];
                toAdd.Package = packageId;
                path.Add(toAdd);
            }
            
            conn.Close();
            result.Close();
            return path;
        }

        public Status getStatus(List<Status> path) {
            if (path == null) {
                return null;
            }
            Status toReturn = path[0];
            foreach (Status s in path){
                if (s.DateTime.CompareTo(toReturn.DateTime) > 0) {
                    toReturn = s;
                }
            }
            return toReturn;
        }
    }
}