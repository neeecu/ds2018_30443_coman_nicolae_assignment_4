﻿using System;

namespace WebService1.App_Code.api
{
    public class UserService
    {
        private UserDao dao;

        public UserService() {
            this.dao = new UserDao();
        }


        public string registerUser(AppUser user) {
            return dao.addUser(user);
        }

        public string login(string username, string password) {
            AppUser fromDb = dao.findUserByUsername(username);
            if (fromDb == null) {
                return "There is no user with the given username";
            }

            if (!fromDb.Password.Equals(password)) {
                return "The username and password do not match";
            }

            return fromDb.Role;
        }

        public AppUser findUserByUsername(string username)
        {
            return dao.findUserByUsername(username);
        }

        public AppUser findUserById(int id)
        {
            return dao.findUserById( id);
        }
    }

   
}