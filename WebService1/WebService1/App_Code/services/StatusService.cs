﻿using System.Collections.Generic;

namespace WebService1.App_Code.api
{
    public class StatusService {
        private StatusDao statusDao = new StatusDao();
        private PackageService packageService = new PackageService();

        public Status getStatusForPackage(string packageName) {
            Package fromDb = packageService.findByName(packageName);
            if(fromDb == null) {
                return null;
            }
            List<Status> path = statusDao.getPathForPackage(fromDb.Id);
            return statusDao.getStatus(path);
        }

        public List<Status> getPathForPackage(int packageId) {
            Package fromDb = packageService.findById(packageId);
            if (fromDb == null)
            {
                return null;
            }
            return statusDao.getPathForPackage(fromDb.Id); 
        }
    }
}