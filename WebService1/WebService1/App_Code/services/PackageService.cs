﻿using System;
using System.Collections.Generic;

namespace WebService1.App_Code.api
{
    public class PackageService
    {
        private UserService userService;
        private PackageDao packageDao;

        public PackageService() {
            this.userService = new UserService();
            this.packageDao = new PackageDao(); 
        }
       
        public List<Package> getAllForUser(string username)        {
            AppUser user = userService.findUserByUsername(username);
            if (user == null) {
                return null;
            }

            return packageDao.getAllForUser(username,user.Id);
        }

        public Package findByName(string packageName) {
            return packageDao.findByName(packageName);
        }

        public Package findById(int id)
        {
            return packageDao.findById(id);
        }
    }
}