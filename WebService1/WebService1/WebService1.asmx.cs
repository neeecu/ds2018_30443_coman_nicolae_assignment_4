﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WebService1.App_Code;

namespace WebService1.App_Code.api
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        UserService userService = new UserService();
        PackageService packageService = new PackageService();
        StatusService statusService = new StatusService();

        [WebMethod]
        public string register(string username, string password, string email, string role){
            return userService.registerUser(new AppUser(username, password, email, role));
        }

        [WebMethod]
        public string login(string username, string password) {
            return userService.login(username,password);
        }

        [WebMethod]
        public List<Package> getAllPackages(string username) {

            return packageService.getAllForUser(username);
        }

        [WebMethod]
        public Package findPackage(string packageName)
        {
            return packageService.findByName(packageName);
        }

        [WebMethod]
        public Status checkPackageStatus(string packageName) {
            return statusService.getStatusForPackage(packageName);
        }

        [WebMethod]
        public AppUser getUserById(int id) {
            return userService.findUserById(id);
        }

        [WebMethod]
        public List<Status> getPathForPackage(int packageId) {
            return statusService.getPathForPackage(packageId);
        }

    }

}
